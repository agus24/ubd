<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ViewAndRememberToken extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::table('mhsw', function(Blueprint $table) {
        //     $table->string('remember_token');
        // });

        // Schema::table('dosen', function(Blueprint $table) {
        //     $table->string('remember_token');
        // });

        // Schema::table('karyawan', function(Blueprint $table) {
        //     $table->string('remember_token');
        // });

        DB::statement("CREATE VIEW v_user AS
        SELECT `MhswID` AS `nomor_unik`,
                `Login` AS `Login`,
                `Password` AS `Password`,
                `Nama` AS `nama`,
                `TempatLahir` AS `TempatLahir`,
                `TanggalLahir` AS `TanggalLahir`,
                `Handphone` AS `Handphone`,
                `Email` AS `Email`,
                `Alamat` AS `Alamat`,
                `Kota` AS `Kota`,
                `KodePos` AS `kodepos`,
                `Propinsi` AS `propinsi`,
                `Negara` AS `negara`,
                'mhsw' AS `tipe`
                -- ,`remember_token` AS `remember_token`
            from `mhsw`
        union select `dosen`.`NIDN` AS `nomor_unik`,
                `dosen`.`Login` AS `Login`,
                `dosen`.`Password` AS `Password`,
                `dosen`.`Nama` AS `Nama`,
                `dosen`.`TempatLahir` AS `TempatLahir`,
                `dosen`.`TanggalLahir` AS `TanggalLahir`,
                `dosen`.`Handphone` AS `Handphone`,
                `dosen`.`Email` AS `Email`,
                `dosen`.`Alamat` AS `Alamat`,
                `dosen`.`Kota` AS `Kota`,
                `dosen`.`KodePos` AS `kodepos`,
                `dosen`.`Propinsi` AS `propinsi`,
                `dosen`.`Negara` AS `negara`,
                'dosen' AS `tipe`
                -- ,`dosen`.`remember_token` AS `remember_token`
            from `dosen`
        union SELECT Login as nomor_unik,
                Login,
                Password,
                Nama,
                '' as TempatLahir,
                '0000-00-00' as TanggalLahir,
                Handphone,
                 Email,
                Alamat,
                Kota,
                '' as KodePos,
                Propinsi,
                Negara,
                'karyawan' as tipe
                -- ,remember_token
            FROM `karyawan`"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('mhsw', function(Blueprint $table) {
        //     $table->dropColumn('remember_token');
        // });

        // Schema::table('dosen', function(Blueprint $table) {
        //     $table->dropColumn('remember_token');
        // });

        // Schema::table('karyawan', function(Blueprint $table) {
        //     $table->dropColumn('remember_token');
        // });

        DB::statement("DROP VIEW v_user");
    }
}
