<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type, X-Requested-With, X-XSRF-TOKEN, Authorization');
header('Access-Control-Allow-Methods: GET,PUT,POST,DELETE');

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

Route::post('token', function(Request $request) {
    $user = App\model\User::find($request->input('username'));
    if($user != null)
    {
        // if($user->cekPassword($request->input('password'))->exists())
        if($user->where('Login',$request->input('username'))->where('Password',DB::raw("left(password('{$request->input('password')}'),10)"))->where('tipe','mhsw')->exists())
        {
            $text_to_encrypt = $request->input('username')."|".Carbon\Carbon::now()->addHours(1)."|".$user->tipe;

            $token = Crypt::encrypt($text_to_encrypt);

            return response()->json(["token" => $token]);
        }
        return response()->json('Wrong Password',403);
    }
    return response()->json('Wrong Username',403);
});

Route::post('token/dosen', function(Request $request) {
    $user = App\model\User::find($request->input('username'));
    if($user != null)
    {
        if($user->where('Login',$request->input('username'))
                ->where('Password',DB::raw("left(password('{$request->input('password')}'),10)"))
                ->where('tipe', 'dosen')
                ->exists())
        {
            $text_to_encrypt = $request->input('username')."|".Carbon\Carbon::now()->addHours(1)."|".$user->tipe;

            $token = Crypt::encrypt($text_to_encrypt);

            return response()->json(["token" => $token]);
        }
        return response()->json('Wrong Password',403);
    }
    return response()->json('Wrong Username',403);
});


Route::post('getUserData', "Api\\ApiController@getUserData");
Route::post('getKRS', "Api\\ApiController@getKRS");
Route::post('getJadwal',"Api\\ApiController@getJadwal");
Route::post('getJadwalUjian',"Api\\ApiController@getJadwalUjian");
Route::post('gantiPass',"Api\\ApiController@gantiPass");
Route::post('pengumuman',"Api\\ApiController@pengumuman");

Route::post('formKalender',"Api\\ApiController@formKalender");
Route::post('pesan',"Api\\ApiController@pesan");

Route::group(['prefix' => "dosen"], function() {
    Route::post('getJadwal', "Api\\ApiController@getJadwalDosen");
    Route::post('getDataDosen', "Api\\ApiController@getDataDosen");
    Route::post('getJadwalUjian', "Api\\ApiController@getJadwalUjianDosen");
    Route::post('getNilaiMatkul', "Api\\ApiController@getNilaiMatkulDosen");
    Route::post('getTugasAkhir', "Api\\ApiController@tugasAkhirDosen");
    Route::post('getPengujiSkripsi', "Api\\ApiController@getPengujiSkripsiDosen");
});
