<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', "HomeController@index");

Route::get('login', 'LoginController@showLogin');
Route::post('/login', 'LoginController@login');
Route::get('/logout', function() {
    Session::flush();
    return redirect('login');
});

Route::group(['prefix' => "info"], function() {
    Route::get('akademik', 'base\\PengumumanController@akademik');
    Route::get('keuangan', 'base\\PengumumanController@keuangan');
    Route::get('perkuliahan', 'base\\PengumumanController@perkuliahan');
});

Route::group(["prefix" => "mahasiswa"], function() {
    Route::get('data', "mhsw\\MahasiswaController@data");
    Route::get('krs', "mhsw\\MahasiswaController@krs");
    Route::get('jadwalUjian', "mhsw\\MahasiswaController@jadwalUjian");
    Route::get('nilaiSemester', "mhsw\\MahasiswaController@nilaiSemester");
});

Route::group(["prefix" => "dosen"], function() {
    Route::get('data', "dsn\\DosenController@data");
    Route::get('jadwalMengajar', "dsn\\DosenController@jadwalMengajar");
    Route::get('jadwalUjian', "dsn\\DosenController@jadwalUjian");
    Route::get('nilaiMatakuliah', "dsn\\DosenController@nilaiMatakuliah");
});

Route::post('/ajax',"AjaxController@requestHandler");

Route::get('/home', 'HomeController@index');
Route::get('/test', function() {
    dd(Session::all());
});
