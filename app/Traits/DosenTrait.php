<?php

namespace App\Traits;

use App\model\Dosen;
use App\model\Jadwal;
use App\model\TugasAkhir;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

trait DosenTrait
{
    public function getJadwalDosen(Request $request)
    {
        $jadwal = (new Jadwal)->getJadwalDosenByUsername($this->username);
        return response()->json($jadwal);
    }

    public function getDataDosen(Request $request)
    {
        $dosen = new Dosen;
        $dosen = $dosen->find($this->username);
        return response()->json($dosen);
    }

    public function getJadwalUjianDosen(Request $request)
    {
        $jadwal = (new Jadwal)->getJadwalUjianDosen($this->username);
        return $jadwal;
    }

    public function getNilaiMatkulDosen(Request $request)
    {
        $jadwalID = $request->jadwal;

        $data = DB::table('krs')
                    ->join('mhsw','krs.MhswID','mhsw.Login')
                    ->select('krs.*','mhsw.nama as nama_mahasiswa')
                    ->where('JadwalID',$jadwalID)
                    ->get();
        return $data;
    }

    public function tugasAkhirDosen(Request $request)
    {
        $ta = (new TugasAkhir)->getForDosen($this->username);
        return $ta;
    }

    public function getPengujiSkripsiDosen(Request $request)
    {
        $penguji = (new TugasAkhir)->getPenguji($this->username);
        return $penguji;
    }
}
