<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Pengumuman extends Model
{
    public function scopegetAll()
    {
        $infoAkademik = DB::table('infakademik')->orderby('tanggal','desc')->limit(10)->get();
        $infoKeuangan = DB::table('infkeuangan')->orderby('tanggal','desc')->limit(10)->get();
        $infoPerkuliahan = DB::table('infperkuliahan')->orderby('tanggal','desc')->limit(10)->get();

        return [
            "infoAkademik" => $infoAkademik,
            "infoKeuangan" => $infoKeuangan,
            "infoPerkuliahan" => $infoPerkuliahan,
        ];
    }

    public function scopegetAkademik()
    {
        return DB::Table('infakademik')->select('id_infoakademik as id','judul_infoakademik as judul','isi_infoakademik as isi','penulis','tanggal')->orderby('tanggal','desc');
    }

    public function scopegetKeuangan()
    {
        return DB::Table('infkeuangan')->select('id_infokeuangan as id','judul_infokeuangan as judul','isi_infokeuangan as isi','penulis','tanggal')->orderby('tanggal','desc');
    }

    public function scopegetPerkuliahan()
    {
        return DB::Table('infperkuliahan')->select('id_infoperkuliahan as id','judul_infoperkuliahan as judul','isi_infoperkuliahan as isi','penulis','tanggal')->orderby('tanggal','desc');
    }
}
