<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TugasAkhir extends Model
{
    protected $table = "ta";
    protected $primaryKey = "TAID";

    public function getForDosen($dosen_id)
    {
        return $this->where('Pembimbing', $dosen_id)->orderby('TahunID', 'desc')->get();
    }

    public function getPenguji($dosen_id)
    {
        $data = DB::table('tadosen')
                    ->join('ta','tadosen.TAID','ta.TAID')
                    ->join('mhsw','tadosen.MhswID','mhsw.Login')
                    ->select('tadosen.*', 'mhsw.nama as nama_mahasiswa','ta.Judul','ta.TglUjian as tanggal_sidang')
                    ->orderby('ta.TglUjian','desc')
                    ->where('DosenID', $dosen_id)
                    ->get();
        return $data;
    }
}
