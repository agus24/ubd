<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Pesan extends Model
{
    protected $table = "pesan";
    protected $primaryKey = 'PesanID';

    public function getPesan($pengirim,$penerima)
    {
        return $this->where('IDPengirim','=', $pengirim)
                    ->where('IDPenerima','=', $penerima)
                    ->select('IDPengirim','IDPenerima','IsiPesan','JudulPesan','TglPesan')
                    ->orderby('TglPesan','asc');
    }
}

