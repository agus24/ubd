<?php

namespace App\model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Jadwal extends Model
{
    protected $table = "jadwal";
    protected $primaryKey = "JadwalID";

    public function getJadwalDosenByUsername($dosen_id)
    {
        $tahun = Carbon::now()->year-4;
        $tahun .= "2";
        return $this->where('DosenID',$dosen_id)
                    ->where('TahunID', '>', $tahun)
                    ->orderby('TahunID','desc')
                    ->get();
    }

    public function getJadwalUjianDosen($dosen_id)
    {
        $jadwal_dosen = $this->getJadwalDosenByUsername($dosen_id)->map(function($value, $key) {
            return $value->JadwalID;
        });

        $uts = DB::table('jadwaluts')->wherein('JadwalID',$jadwal_dosen)->select('*',DB::raw("'uts' as tipe"))->get();
        $uas = DB::table('jadwaluas')->wherein('JadwalID',$jadwal_dosen)->select('*',DB::raw("'uas' as tipe"))->get();
        $data = $uts->union($uas);
        return $data;
    }
}
