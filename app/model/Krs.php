<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Krs extends Model
{
    protected $table = "krs";
    protected $primaryKey = "KRSID";

    //GETTER
    public function getData($username)
    {
        return $this->data($username)->get();
    }

    public function getDataDetail($username)
    {
        return $this->dataDetail($username)->get();
    }

    public function getJadwal($username)
    {
        return $this->jadwal($username)->get();
    }

    public function getJadwalUjian($username,$tipe,$tahun=null)
    {
        if($tahun == null)
        {
            return $this->jadwalUjian($username,$tipe)->select('jadwal'.$tipe.".*",'jadwal.Nama','jadwal.MKKode')
                        ->get();
        }
        return $this->jadwalUjian($username,$tipe)->where('jadwal.TahunID',$tahun)->select('jadwal'.$tipe.".*",'jadwal.Nama','jadwal.MKKode')
                    ->get();
    }

    //QUERY
    private function jadwal($username)
    {
        $a = $this->join('jadwal','krs.JadwalID','jadwal.JadwalID')
                    ->join('dosen','jadwal.DosenID','dosen.Login')
                    ->where('MhswID',$username)
                    ->select('jadwal.*','dosen.Nama as namaDosen','krs.CatatanError as CatatanError','krs.StatusKRSID')
                    ->orderby('jadwal.TahunID')
                    ->orderby('jadwal.HariID')
                    ->orderby('jadwal.JamMulai');
        return $a;
    }

    private function jadwalUjian($username,$tipe)
    {
        $jadwal = $this->join('jadwal','krs.JadwalID','jadwal.JadwalID')
                    ->join('dosen','jadwal.DosenID','dosen.Login')
                    ->where('MhswID',$username);
        return $jadwal->join('jadwal'.$tipe,'jadwal'.$tipe.".JadwalID","jadwal.JadwalID")
                        ->orderby('jadwal'.$tipe.'.Tanggal')
                        ->orderby('jadwal'.$tipe.'.JamMulai');
    }

    private function dataDetail($username)
    {
        return $this->where('MhswID',$username)
                    ->orderby('TahunID','asc')
                    ->orderby('MKID','asc');
    }

    private function data($username)
    {
        return DB::table('khs')->where('MhswID',$username)
                                ->orderby('sesi');
    }

}
