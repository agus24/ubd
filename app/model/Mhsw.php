<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Mhsw extends Model
{
    protected $table = 'mhsw';
    protected $primaryKey = 'Login';

     protected $hidden = [
        'Password',
        'LevelID',
        'id_reg_pd',
        'id_pd',
        'KDPIN',
        'PMBID',
        'PMBFormJualID',
        'PSSBID',
        'NIRM',
        'NIMAN',
        'LoginBuat',
        'TanggalBuat',
        'LoginEdit',
        'TanggalEdit',
        'flag_login',
        'faillog',
        'iplog',
        'cookieslog',
        'Syn',
        'Syn2',
        'Syn3'
    ];

    public function program()
    {
        return $this->belongsTo('App\model\relation\Program','ProgramID','ProgramID');
    }

    public function prodi()
    {
        return $this->belongsTo('App\model\relation\Prodi','ProdiID','ProdiID');
    }

    public function wali()
    {
        return $this->belongsTo("App\model\Dosen",'PenasehatAkademik','Login');
    }
}
