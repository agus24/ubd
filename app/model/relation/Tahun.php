<?php

namespace App\model\relation;

use Illuminate\Database\Eloquent\Model;

class Tahun extends Model
{
    protected $table = "tahun";
    protected $primaryKey = "TahunID";
}
