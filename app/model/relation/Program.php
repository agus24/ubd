<?php

namespace App\model\relation;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $table = "program";
    protected $primaryKey = "ProgramID";
}
