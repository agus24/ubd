<?php

namespace App\model\relation;

use Illuminate\Database\Eloquent\Model;

class Prodi extends Model
{
    protected $table = "prodi";
    protected $primaryKey = "ProdiID";
}
