<?php

namespace App\model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = "v_user";
    protected $primaryKey = "Login";
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama', 'Login', 'Password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'Password',
        // 'LevelID',
        // 'id_reg_pd',
        // 'id_pd',
        // 'KDPIN',
        // 'PMBID',
        // 'PMBFormJualID',
        // 'PSSBID',
        // 'NIRM',
        // 'NIMAN',
        // 'LoginBuat',
        // 'TanggalBuat',
        // 'LoginEdit',
        // 'TanggalEdit',
        // 'flag_login',
        // 'faillog',
        // 'iplog',
        // 'cookieslog',
        // 'Syn',
        // 'Syn2',
        // 'Syn3'
    ];

    public $remember_token;

    public function cekPassword($password)
    {
        return $this->where('Login',$this->Login)->where('Password',DB::raw("left(password('$password'),10)"));
    }

    public function getData($username)
    {
        return $this->where('Login' , $username)->select('Login as Kode', 'nama');
    }

    public function gantiPass($password)
    {
        $this->where('Login',$this->Login)->update(["password" => $this->encrypt($password)]);
    }

    private function encrypt($text)
    {
        $pass = strtoupper(
                sha1(
                        sha1($text, true)
                )
        );
        $pass = '*' . $pass;
        return substr($pass,0,10);
    }
}
