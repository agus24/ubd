<?php

namespace App\Http;

use App\model\Pengumuman;
use Illuminate\Contracts\View\View;

class ViewComposer
{
    public function compose(View $view)
    {
        $view->with('pengumuman', Pengumuman::getAll());
    }
}
