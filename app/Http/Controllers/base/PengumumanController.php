<?php

namespace App\Http\Controllers\base;

use App\Http\Controllers\Controller;
use App\model\Pengumuman;
use Illuminate\Http\Request;

class PengumumanController extends Controller
{

    public function __construct()
    {
        $this->middleware('login');
    }

    public function akademik()
    {
        $pengumumans = Pengumuman::getAkademik()->paginate(10);
        return view('pengumuman.base',['pengumumans' => $pengumumans,"judul" => "Akademik"]);
    }

    public function keuangan()
    {
        $pengumumans = Pengumuman::getKeuangan()->paginate(10);
        // dd($pengumumans);
        return view('pengumuman.base',['pengumumans' => $pengumumans,"judul" => "Keuangan"]);
    }

    public function perkuliahan()
    {
        $pengumumans = Pengumuman::getPerkuliahan()->paginate(10);
        return view('pengumuman.base',['pengumumans' => $pengumumans,"judul" => "Perkuliahan"]);
    }
}
