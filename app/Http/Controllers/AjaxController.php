<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\model\Pengumuman;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function requestHandler(Request $request)
    {
        if(!$request->ajax())
        {
            return response()->json("no direct link allowed",403);
        }

        switch($request->input('request'))
        {
            case "pengumuman":
                return response()->json($this->pengumuman($request->input('tipe')),200);
                break;
        }

    }

    private function pengumuman($tipe)
    {
        switch($tipe)
        {
            case "Akademik":
                return Pengumuman::getAkademik()->get();
                break;
            case "Keuangan":
                return Pengumuman::getKeuangan()->get();
                break;
            case "Perkuliahan":
                return Pengumuman::getPerkuliahan()->get();
                break;
            default:
                return response()->json("Type Not Found",404);
                break;
        }
    }
}
