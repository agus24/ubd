<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Crypt;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $tipe;
    protected $username;
    protected $time;

    protected function parseToken($token)
    {
        $decToken = Crypt::decrypt($token);
        $ex = explode("|",$decToken);
        $this->username = $ex[0];
        $this->time = Carbon::createFromFormat('Y-m-d H:i:s', $ex[1]);
        $this->tipe = $ex[2];
        // $this->time = Carbon::createFromFormat('Y-m-d H:i:s', $this->time);
    }
}
