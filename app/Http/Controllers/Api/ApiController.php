<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Traits\DosenTrait;
use App\model\Krs;
use App\model\Mhsw;
use App\model\Pengumuman;
use App\model\Pesan;
use App\model\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class ApiController extends Controller
{
    use DosenTrait;
    private $krs;

    private $agama;
    private $statusSipil;

    public function __construct(Request $request)
    {
        if(! $request->input('token') || empty($request->input('token')))
        {
            return response()->json(['token required'], 403);
        }

        $this->parseToken($request->input('token'));
        /*
        if($this->time < Carbon::now()){
            return response()->json(['Token Expired'],402);
        }
        */

        $this->krs = new Krs;

        $this->agama = [
            'B' => 'Budha',
            'G' => 'Protestan',
            'H' => 'Hindu',
            'I' => 'Islam',
            'K' => 'Katholik',
            'KH' => 'Konghucu',
            'KR' => 'Kristen',
            'L' => 'Lain2',
            'P' => 'Protestan Non GKI'
        ];

        $this->statusSipil = [
            'B' => "Belum Menikah",
            "K" => "Menikah",
            "D" => "Duda/Janda"
        ];
    }

    public function getUserData(Request $request)
    {
        $user = Mhsw::find($this->username);
        $data = $user->toArray();
        $data['namaProgram'] = $user->program->Nama;
        $data['namaProdi'] = $user->prodi->Nama;
        $data['wali'] = $user->wali->Nama;
        $data['Agama'] = $this->agama[$user->Agama];
        $data['AgamaAyah'] = $this->agama[$user->AgamaAyah];
        $data['AgamaIbu'] = $this->agama[$user->AgamaIbu];
        $data['StatusSipil'] = $this->statusSipil[$user->StatusSipil];
        return response()->json($data,200);
    }

    public function getKRS(Request $request)
    {

        $output = [
            "header" => $this->krs->getData($this->username),
            "detail" => $this->krs->getDataDetail($this->username)
        ];
        return response()->json($output,200);
    }

    public function getJadwal(Request $request)
    {
        $jadwal = $this->krs->getJadwal($this->username);

        return response()->json($jadwal,200);
    }

    public function getJadwalUjian(Request $request)
    {
        return $this->krs->getJadwalUjian($this->username,$request->input('tipe'));
    }

    public function gantiPass(Request $request)
    {
        $user = User::find($this->username);
        $user->gantiPass($request->input('password'));
        // $user->Password = bcrypt($request->input('password'));
        // $user->save();
        return response()->json(['password changed'],202);
    }

    public function pengumuman(Request $request)
    {
        $output = [
            "akademik" => Pengumuman::getAkademik()->get(),
            "keuangan" => Pengumuman::getKeuangan()->get(),
            "perkuliahan" => Pengumuman::getPerkuliahan()->get(),
        ];

        return response()->json($output,200);
    }


    public function formKalender(Request $request)
    {
        $baseLink = "http://sia.ubd.ac.id/";
        return response()->json(
                    $this->generateFormKalender($baseLink)
                ,200);
    }

    public function pesan(Request $request)
    {
        $pengirim = $request->input('pengirim');
        $penerima = $request->input('penerima');
        $pesan = (new Pesan)->getPesan($pengirim,$penerima)->get();

        $pesan = $pesan->map(function($item, $key) {
            $item->IDPengirim = (new User)->getData($item->IDPengirim)->first();
            $item->IDPenerima = (new User)->getData($item->IDPenerima)->first();
            return $item;
        });

        return response()->json($pesan,200);

    }

    private function generateFormKalender($baseLink)
    {
        return [
                    [
                        "judul" => "Form Aktif Kuliah" ,
                        "link" => $baseLink."form/FORM Aktif Kuliah.docx?8f7id13v7smfehoefdg06cne54",
                    ],
                    [
                        "judul" => "Form Biodata Mahasiswa" ,
                        "link" => $baseLink."form/FORM Biodata Mahasiswa.docx?8f7id13v7smfehoefdg06cne54",
                    ],
                    [
                        "judul" => "Form Cuti Kuliah" ,
                        "link" => $baseLink."form/FORM Cuti Kuliah.docx?8f7id13v7smfehoefdg06cne54",
                    ],
                    [
                        "judul" => "Form Konsentrasi" ,
                        "link" => $baseLink."form/FORM Konsentrasi.docx?8f7id13v7smfehoefdg06cne54",
                    ],
                    [
                        "judul" => "Form Pindah Konsentrasi" ,
                        "link" => $baseLink."form/FORM Pindah Konsentrasi.docx?8f7id13v7smfehoefdg06cne54",
                    ],
                    [
                        "judul" => "Form KRS Manual" ,
                        "link" => $baseLink."form/FORM KRS Manual.docx?8f7id13v7smfehoefdg06cne54",
                    ],
                    [
                        "judul" => "Form KRS Semester Pendek" ,
                        "link" => $baseLink."form/FORM KRS Semester Pendek.docx?8f7id13v7smfehoefdg06cne54",
                    ],
                    [
                        "judul" => "Form Pindah Program Studi" ,
                        "link" => $baseLink."form/FORM Pindah Program Studi.docx?8f7id13v7smfehoefdg06cne54",
                    ],
                    [
                        "judul" => "Form Pindah Waktu Kuliah" ,
                        "link" => $baseLink."form/FORM Pindah Waktu Kuliah.docx?8f7id13v7smfehoefdg06cne54",
                    ],
                    [
                        "judul" => "Form Pengajuan Judul Skripsi / TA" ,
                        "link" => $baseLink."form/FORM PENGAJUAN TA dan SKRIPSI.pdf?8f7id13v7smfehoefdg06cne54",
                    ],
                    [
                        "judul" => "Kartu Bimbingan Kerja Praktek / Proyek Minor" ,
                        "link" => $baseLink."form/Kartu Bimbingan Kerja Praktek.docx?8f7id13v7smfehoefdg06cne54",
                    ],
                    [
                        "judul" => "Form Penggantian Judul Skripsi / TA" ,
                        "link" => $baseLink."form/FORM Penggantian Judul Skripsi TA.docx?8f7id13v7smfehoefdg06cne54",
                    ],
                    [
                        "judul" => "Form Perpanjangan Skripsi / TA" ,
                        "link" => $baseLink."form/FORM Perpanjangan Skripsi TA.docx?8f7id13v7smfehoefdg06cne54",
                    ],
                    [
                        "judul" => "Form Pendaftaran Sidang Skripsi / TA" ,
                        "link" => $baseLink."form/FORM Pendaftaran Sidang Skripsi TA.docx?8f7id13v7smfehoefdg06cne54",
                    ],
                    [
                        "judul" => "Form Kartu Mahasiswa" ,
                        "link" => $baseLink."form/FORM Kartu Mahasiswa.docx?8f7id13v7smfehoefdg06cne54",
                    ],
                    [
                        "judul" => "Form Tidak Masuk Kuliah" ,
                        "link" => $baseLink."form/FORM Tidak Masuk Kuliah.docx?8f7id13v7smfehoefdg06cne54",
                    ],
                    [
                        "judul" => "Form Ujian Susulan" ,
                        "link" => $baseLink."form/FORM UJIAN SUSULAN.pdf?8f7id13v7smfehoefdg06cne54",
                    ],
                    [
                        "judul" => "Kalender Akademik 20161 - 20162" ,
                        "link" => $baseLink."form/KALENDER AKADEMIK 20161-20162.xlsx?8f7id13v7smfehoefdg06cne54",
                    ],
            ];
    }
}
