<?php

namespace App\Http\Controllers\mhsw;

use App\Http\Controllers\Controller;
use App\model\Krs;
use App\model\Mhsw;
use App\model\relation\Tahun;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Session;

class MahasiswaController extends Controller
{

    private $agama;
    private $statusSipil;
    private $hari;

    /**
     * MahasiswaController constructor.
     */
    public function __construct()
    {
        $this->middleware('login');
        $this->agama = [
            'B' => 'Budha',
            'G' => 'Protestan',
            'H' => 'Hindu',
            'I' => 'Islam',
            'K' => 'Katholik',
            'KH' => 'Konghucu',
            'KR' => 'Kristen',
            'L' => 'Lain2',
            'P' => 'Protestan Non GKI'
        ];

        $this->statusSipil = [
            'B' => "Belum Menikah",
            "K" => "Menikah",
            "D" => "Duda/Janda"
        ];

        $this->hari = [
            "Minggu",
            "Senin",
            "Selasa",
            "Rabu",
            "Kamis",
            "Jumat",
            "Sabtu",
        ];
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function data()
    {
        $user = Session::get('user');
        $mhsw = Mhsw::find($user->nomor_unik);
        $krs_detail = (new Krs)->getDataDetail($user->nomor_unik);
        $krs = (new Krs)->getData($user->nomor_unik);
        // dd($krs);
        $dataKrs = [];
        foreach($krs as $key => $value)
        {
            $dataKrs[] = [
                "semester" => $value->TahunID,
                "data" => $krs_detail->where('TahunID',$value->TahunID),
                "IPS" => $value->IP,
                "SKS" => $value->SKS,
            ];
        }
        // dd($dataKrs);

        return view(
                    'mahasiswa.data',
                    [
                        'mhsw' => $mhsw,
                        'agama' => $this->agama,
                        'statusSipil' => $this->statusSipil,
                        'krs' => $dataKrs,
                        'krs_detail' => $krs_detail,
                        'krs_header' => $krs,
                    ]
                );
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function krs()
    {
        if(empty($_GET['tahun']))
        {
            $tahun = array_values(Tahun::orderby('TahunID','desc')->select('TahunID')->first()->toArray())[0];
        }
        else{
            $tahun = $_GET['tahun'];
        }
        $user = Session::get('user');
        $mhsw = Mhsw::find($user->nomor_unik);
        $krs = (new Krs)->getJadwal($user->nomor_unik);
        $krs = $krs->groupBy('TahunID')->reverse();
        // dd($krs);

        return view('mahasiswa.krs',['krs' => $krs[$tahun],'tahun' => $tahun,'allTahun' => $krs->keys(),'hari' => $this->hari]);
    }

    public function jadwalUjian()
    {
        $tahun = !empty($_GET['tahun']) ? $_GET['tahun'] : $this->getTahun();

        $tahunOpt = (new Krs)->getJadwal(Session::get('user')->nomor_unik)->groupBy('TahunID')->reverse()->keys();

        $jadwal = [
            "uts" => $this->mapJadwalTanggal((new Krs)->getJadwalUjian(Session::get('user')->nomor_unik,'uts',$tahun)),
            "uas" => $this->mapJadwalTanggal((new Krs)->getJadwalUjian(Session::get('user')->nomor_unik,'uas',$tahun)),
        ];

        return view('mahasiswa.ujian',['jadwal' => $jadwal,'allTahun' => $tahunOpt, 'tahun' => $tahun,'hari' => $this->hari]);
    }

    public function nilaiSemester()
    {
        if(empty($_GET['tahun']))
        {
            $tahun = array_values(Tahun::orderby('TahunID','desc')->select('TahunID')->first()->toArray())[0];
        }
        else{
            $tahun = $_GET['tahun'];
        }
        $user = Session::get('user');
        $mhsw = Mhsw::find($user->nomor_unik);
        $nilai = (new Krs)->getDataDetail($user->nomor_unik)->groupby('TahunID')->reverse();
        // dd($nilai);

        return view('mahasiswa.nilai',[
                            "nilai" => $nilai[$tahun],'tahun' => $tahun,'allTahun' => $nilai->keys()]);
    }

    private function mapJadwalTanggal($data)
    {
        $dayList = array(
            'Sun' => 'Minggu',
            'Mon' => 'Senin',
            'Tue' => 'Selasa',
            'Wed' => 'Rabu',
            'Thu' => 'Kamis',
            'Fri' => 'Jumat',
            'Sat' => 'Sabtu'
        );

        $data = $data->map(function($item,$key) use($dayList){
            $hari = Carbon::createFromFormat('Y-m-d', $item->Tanggal)->format('D');
            $item->Tanggal = $dayList[$hari].", ".Carbon::createFromFormat('Y-m-d', $item->Tanggal)->format('d-m-Y');
            return $item;
        });
        return $data;
    }

    private function getTahun()
    {
        return array_values(Tahun::orderby('TahunID','desc')->select('TahunID')->first()->toArray())[0];
    }
}
