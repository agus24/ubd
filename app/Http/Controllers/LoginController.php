<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\model\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Session;

class LoginController extends Controller
{
    public function __construct()
    {
        // $this->middleware('login');
    }

    public function showLogin()
    {
        return view('login');
    }

    public function login(Request $request)
    {
        $this->validate($request,[
            "username" => "required",
            "password" => "required"
        ]);

        $username = $request->input('username');
        $password = $request->input('password');

        $mhsw = User::find($username);
        if($mhsw != null)
        {
            if(null != User::where('Login',$username)->where('password',DB::raw("left(password('$password'),10)"))->first())
            {
                Session::put('user',$mhsw);
                return redirect('home');
            }
            return redirect('login')->withErrors(['password'=>'Password Salah']);
        }
        return redirect('login')->withErrors(['username'=>'Username Tidak Ditemukan']);
    }
}
