@extends('layouts.app')

@section('login')
<br>
<br>
<section class="section-account">
    <div class="card contain-sm">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <br/>
                    <span class="text-lg text-bold text-primary"> Login</span>
                    <br/><br/>

                    <form class="form floating-label" action="{{ url('login') }}" accept-charset="utf-8" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group {{ $errors->has('username')? "has-error" : "" }}">
                            <input type="text" class="form-control" id="username" name="username" value="" autocomplate="off">
                            <label for="username">Username</label>
                            {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group {{ $errors->has('password')? "has-error" : "" }}">
                            <input type="password" class="form-control" id="password" name="password" value="" autocomplate="off">
                            <label for="password">Password</label>
                            {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-xs-12 text-right">
                                <button class="btn btn-primary btn-raised" type="submit">Login</button>
                            </div><!--end .col -->
                        </div><!--end .row -->
                    </form>
                </div><!--end .col -->
            </div><!--end .row -->
        </div><!--end .card-body -->
    </div><!--end .card -->
</section>
@endsection
