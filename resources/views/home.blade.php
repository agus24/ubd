@extends('layouts.app')
@section('content')
<br>
<div class="row">
    <div class="col-md-offset-3 col-md-6">
        <div class="card card-bordered card-underline style-primary">
            <div class="card-head">
                <header>Selamat Datang {{ Session::get('user')->nama }}</header>
                <div class="tools">
                    <div class="btn-group">
                        <a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
                        <a class="btn btn-icon-toggle btn-close"><i class="md md-close"></i></a>
                    </div>
                </div>
            </div>
            <div class="card-body style-default-bright">
                <ul class="list divider-full-bleed">
                    <li class="tile">
                        <a class="tile-content ink-reaction">
                            <div class="tile-text">
                                Login
                                <small class="text-bold">
                                    {{ Session::get('user')->nomor_unik }}
                                </small>
                            </div>
                        </a>
                    </li>
                    <li class="tile">
                        <a class="tile-content ink-reaction">
                            <div class="tile-text">
                                Nama
                                <small class="text-bold">
                                    {{ Session::get('user')->nama }}
                                </small>
                            </div>
                        </a>
                    </li>
                    <li class="tile">
                        <a class="tile-content ink-reaction" href="{{ url('logout') }}">
                            <div class="tile-text">
                                <b>Logout</b>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card card-underline style-primary">
            <div class="card-head">
                <header>
                    Pengumuman
                    <span id="judul"></span>
                    <button class="btn ink-reaction btn-floating-action btn-xs btn-primary" id="cycleLeft" disabled="disabled"><i class="md md-keyboard-arrow-left"></i></button>
                    <button class="btn ink-reaction btn-floating-action btn-xs btn-primary" id="cycleRight"><i class="md md-keyboard-arrow-right"></i></button>
                </header>
                <div class="tools">
                    <div class="btn-group">
                        <a class="btn btn-icon-toggle btn-refresh" id="ref"><i class="md md-refresh"></i></a>
                        <a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
                        <a class="btn btn-icon-toggle btn-close"><i class="md md-close"></i></a>
                    </div>
                </div>
            </div><!--end .card-head -->
            <div class="card-body style-default-bright" id="cardBody">
                <table class="table table-bordered table-stripped datatable" id="tablePengumuman">
                    <thead>
                        <th>Tanggal</th>
                        <th>Judul</th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div><!--end .card-body -->
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    var tipe = ['Akademik','Keuangan','Perkuliahan'];
    var angka = 0;
    var angka_max = 2;
    var isi = [];
    var judul = [];
    generateTable(tipe[0]);
    $('#cycleRight').on('click', function() {
        buttonCheck(++angka);
        generateTable(tipe[angka]);
    });

    $('#cycleLeft').on('click', function() {
        buttonCheck(--angka);
        generateTable(tipe[angka]);
    });

    function buttonCheck(angka)
    {
        if(angka == 0)
        {
            $('#cycleRight').prop('disabled',false);
            $('#cycleLeft').prop('disabled',true);
        }
        else if(angka == angka_max)
        {
            $('#cycleRight').prop('disabled',true);
            $('#cycleLeft').prop('disabled',false);
        }
        else{
            $('#cycleRight').prop('disabled',false);
            $('#cycleLeft').prop('disabled',false);
        }
    }

    function generateTable(tipe)
    {
        $.ajax({
            url : "{{ url('ajax') }}",
            data : {
                "_token" : Laravel._token,
                "request" : "pengumuman",
                "tipe" : tipe
            },
            type : "POST",
            success : function(result) {
                var html = '';
                $.each(result, function(key, value) {
                    html += "<tr>";
                    html += "<td>";
                    html += value.tanggal;
                    html += "</td>";
                    html += "<td>";
                    html += "<button class='btn btn-primary' onclick='openCanvas("+value.id+")'>"+value.judul+'</button>';
                    html += "</td>";
                    html += "</tr>";
                    isi[value.id] = value.isi;
                    judul[value.id] = value.judul;
                });
                $('#tablePengumuman > tbody').empty();
                $('#tablePengumuman > tbody').append(html);
                $('#judul').empty();
                $('#judul').text(tipe);
                $('#tablePengumuman').DataTable();
            }
        });
    }

    function openCanvas(id)
    {
        $('#judulModal').empty();
        $('#judulModal').append(judul[id]);
        $('#isiModal').empty();
        $('#isiModal').append(isi[id]);
        $('#pengumumanModal').modal('show');
    }
</script>
@endsection

@section('modal')
<div id="pengumumanModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="judulModal"></h4>
      </div>
      <div class="modal-body">
        <span id="isiModal"></span>
      </div>
      <div class="modal-footer">
        {{-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> --}}
      </div>
    </div>

  </div>
</div>
@endsection
