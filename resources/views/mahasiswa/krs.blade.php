@extends('layouts.app')
@section('style')
<style>

</style>
@endsection
@section('content')
<br>
<div class="row">
    <div class="col-md-12">
    <h1 class="text-primary">Kartu Rencana Studi</h1>
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="tahunID">Semester</label>
                            <select class="form-control" id="tahun">
                                <option value=''>Select</option>
                                @foreach($allTahun as $tahunOption)
                                    <option value="{{$tahunOption}}" {{ $tahunOption == (!empty($_GET['tahun'])? 
					$_GET['tahun'] : false) ? 'selected' : '' }} >{{$tahunOption}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <ul class="list divider-full-bleed">
                            <div class="card panel">
                                <div class="card-head style-primary " data-toggle="" data-parent="#semester{{ $tahun }}" data-target="#semester{{ $tahun }}">
                                    <header>Tahun : {{ $tahun }}</header>
                                </div>
                                <div id="semester{{$tahun}}" class="">
                                    <div class="card-body">
                                        <table class="table table-responsive">
                                        <thead>
                                            <th>No.</th>
                                            <th>Hari</th>
                                            <th>Jam Kuliah</th>
                                            <th>Ruang</th>
                                            <th>Kode</th>
                                            <th>Mata Kuliah</th>
                                            <th>SKS</th>
                                            <th>Dosen</th>
                                            <th>Status</th>
                                            <th>Catatan</th>
                                        </thead>
                                        <tbody>
                                        @foreach($krs as $key=> $jadwal)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td>{{ $hari[$jadwal->HariID] }}</td>
                                            <td>{{ $jadwal->JamMulai." - ".$jadwal->JamSelesai }}</td>
                                            <td>{{ $jadwal->RuangID }}</td>
                                            <td>{{ $jadwal->MKKode }}</td>
                                            <td>{{ $jadwal->Nama }}</td>
                                            <td>{{ $jadwal->SKS }}</td>
                                            <td>{{ $jadwal->namaDosen }}</td>
                                            <td>{{ $jadwal->StatusKRSID }}</td>
                                            <td>{{ $jadwal->CatatanError }}</td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
$('#tahun').on('change', function() {
    var tahun = $(this).val();

    window.location.href = "?tahun="+tahun;
});
</script>
@endsection
