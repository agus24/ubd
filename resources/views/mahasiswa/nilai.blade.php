@extends('layouts.app')

@section('content')
<br>
<h1 class="text-primary">Nilai Semester</h1>
<div class="card">
    <div class="card-body">
        <div class="col-md-4">
            <div class="form-group">
            <label for="select">Tahun</label>
                <select class="form-control" id="select">
                    <option value="">Select</option>
                    @foreach($allTahun as $thn)
                    <option value="{{ $thn }}">{{ $thn }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-12">
            <table class="table table-responsive">
            <thead>
                <th>No.</th>
                <th>Kode</th>
                <th>Nama Matakuliah</th>
                <th>SKS</th>
                <th>Kehadiran</th>
                <th>Tugas 1</th>
                <th>Tugas 2</th>
                <th>Tugas 3</th>
                <th>Presentasi</th>
                <th>Lab.</th>
                <th>UTS</th>
                <th>UAS</th>
                <th>Nilai Akhir</th>
                <th>Grade</th>
                <th>Bobot</th>
            </thead>
            <tbody>
            @foreach($nilai as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $value->MKKode }}</td>
                <td>{{ $value->Nama }}</td>
                <td>{{ $value->SKS }}</td>
                <td>{{ $value->_Presensi }}</td>
                <td>{{ $value->Tugas1 }}</td>
                <td>{{ $value->Tugas2 }}</td>
                <td>{{ $value->Tugas3 }}</td>
                <td>{{ $value->Tugas4 }}</td>
                <td>{{ $value->Tugas5 }}</td>
                <td>{{ $value->UTS }}</td>
                <td>{{ $value->UAS }}</td>
                <td>{{ $value->NilaiAkhir }}</td>
                <td>{{ $value->GradeNilai }}</td>
                <td>{{ $value->BobotNilai }}</td>
            </tr>
            @endforeach
            </tbody>
            </table>
        </div>
    </div>
</div>


@endsection

@section('script')
<script>
$('#select').on('change', function() {
    window.location.href = "?tahun="+this.value;
});
</script>
@endsection
