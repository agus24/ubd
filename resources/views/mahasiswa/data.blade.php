@extends('layouts.app')
@section('style')
<style>

</style>
@endsection
@section('content')
<br>
<div class="row">
    <div class="col-md-12">
    <h1 class="text-primary">Data Mahasiswa</h1>
        <div class="col-md-offset-2 col-md-8">
            <div class="card">
                <div class="card-body no-padding">
                    <div class="col-md-4">
                        <img src="http://sia.ubd.ac.id/{{ $mhsw->Foto }}" width="200px" height="240px" style="padding-top:10px;padding-bottom:1px">
                    </div>
                    <div class="col-md-8">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td>Nim</td>
                                    <td>{{$mhsw->MhswID}}</td>
                                </tr>
                                <tr>
                                    <td>Nama</td>
                                    <td>{{$mhsw->Nama}}</td>
                                </tr>
                                <tr>
                                    <td>Program</td>
                                    <td>{{$mhsw->program->Nama}}({{$mhsw->ProgramID}})</td>
                                </tr>
                                <tr>
                                    <td>Program Studi</td>
                                    <td>{{$mhsw->prodi->Nama}}({{$mhsw->ProdiID}})</td>
                                </tr>
                                <tr>
                                    <td>Dosen Wali</td>
                                    <td>{{$mhsw->wali->Nama}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-head">
                    <ul class="nav nav-tabs" data-toggle="tabs">
                        <li class="active"><a href="#tab1">Data Pribadi</a></li>
                        <li><a href="#tab2">Orang Tua</a></li>
                        <li><a href="#tab3">Data Akademik</a></li>
                        <li><a href="#tab4">Data Semester</a></li>
                        <li><a href="#tab5">Data Matakuliah</a></li>
                        <li><a href="#tab6">Prestasi</a></li>
                        <li><a href="#tab7">Tugas Akhir</a></li>
                        <li><a href="#tab8">Keuangan</a></li>
                    </ul>
                </div>
                <div class="card-body tab-content">
                    <div class="card-body tab-content">
                        <div class="tab-pane active" id="tab1">
                            <table class="table table-responsive">
                                <tr>
                                    <td colspan="2"><b>Pribadi</b></td>
                                </tr><tr>
                                    <td>No. KTP</td>
                                    <td>{{ $mhsw->NIK }}
                                </tr><tr>
                                    <td>Jenis Kelamin</td>
                                    <td>{{ $mhsw->Kelamin == 'L' ? 'Laki-Laki' : "Perempuan" }}</td>
                                </tr><tr>
                                    <td>Agama</td>
                                    <td>{{ $agama[$mhsw->Agama] }}</td>
                                </tr><tr>
                                    <td>Tempat, Tgl Lahir</td>
                                    <td>{{ $mhsw->TempatLahir.", ".$mhsw->TanggalLahir }}</td>
                                </tr><tr>
                                    <td>Status Sipil</td>
                                    <td>{{ $statusSipil[$mhsw->StatusSipil] }}</td>
                                </tr><tr>
                                    <td>Warga Negara</td>
                                    <td>{{ $mhsw->WargaNegara }}</td>
                                </tr><tr>
                                    <td colspan="2"><b>Alamat Sesuai KTP</b></td>
                                </tr><tr>
                                    <td>Alamat</td>
                                    <td>{{ $mhsw->Alamat }}</td>
                                </tr><tr>
                                    <td>RT/RW</td>
                                    <td>{{ $mhsw->RT."/".$mhsw->RW }}</td>
                                </tr><tr>
                                    <td>Kota - Kode Pos</td>
                                    <td>{{ $mhsw->Kota." - ".$mhsw->KodePos }}</td>
                                </tr><tr>
                                    <td>Provinsi, Negara</td>
                                    <td>{{ $mhsw->Propinsi.", ".$mhsw->Negara }}</td>
                                </tr><tr>
                                    <td>Telepon, HP</td>
                                    <td>{{ $mhsw->Telepon.", ".$mhsw->Handphone }}</td>
                                </tr><tr>
                                    <td colspan="2"><b>Alamat Tinggal</b></td>
                                </tr><tr>
                                    <td>Alamat</td>
                                    <td>{{ $mhsw->AlamatAsal }}</td>
                                </tr><tr>
                                    <td>RT/RW</td>
                                    <td>{{ $mhsw->RTAsal."/".$mhsw->RWAsal }}</td>
                                </tr><tr>
                                    <td>Kota - Kode Pos</td>
                                    <td>{{ $mhsw->KotaAsal }}</td>
                                </tr><tr>
                                    <td>Provinsi, Negara</td>
                                    <td>{{ $mhsw->PropinsiAsal.", ".$mhsw->NegaraAsal }}</td>
                                </tr><tr>
                                    <td>Telepon</td>
                                    <td>{{ $mhsw->TeleponAsal }}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="tab-pane" id="tab2">
                            {{-- TAB DATA ORANG TUA --}}
                        </div>
                        <div class="tab-pane" id="tab3">
                            {{-- TAB DATA AKADEMIK --}}
                        </div>
                        {{-- DATA SEMESTER --}}
                        <div class="tab-pane" id="tab4">
                            <ul class="list divider-full-bleed">
                            @foreach($krs as $key => $value)
                            <li>
                                <div class="card panel">
                                    <div class="card-head style-primary collapsed" data-toggle="collapse" data-parent="#semester{{ $value['semester'] }}" data-target="#semester{{ $value['semester'] }}">
                                        <header>Semester / TA : {{ $key+1 }} / {{ $value['semester'] }}</header>
                                        <small>SKS/IPS : {{ $value['SKS']."/".$value['IPS'] }}</small>
                                    </div>
                                    <div id="semester{{ $value['semester'] }}" class="collapse">
                                        <div class="card-body">
                                            <table class="table table-responsive">
                                            <thead>
                                                <th>No.</th>
                                                <th>Kode</th>
                                                <th>Mata Kuliah</th>
                                                <th>SKS</th>
                                                <th>Grade</th>
                                                <th>Bobot</th>
                                            </thead>
                                            <tbody>
                                            @foreach($value['data'] as $no => $krs)
                                            <tr>
                                                <td>{{ $no+1 }}.</td>
                                                <td>{{ $krs->MKKode }}</td>
                                                <td>{{ $krs->Nama }}</td>
                                                <td>{{ $krs->SKS }}</td>
                                                <td>{{ $krs->GradeNilai }}</td>
                                                <td>{{ $krs->BobotNilai }}</td>
                                            </tr>
                                            @endforeach
                                            </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div><!--end .panel -->
                            </li>
                            @endforeach
                                <li>
                                    <table class="table table-bordered">
                                    <tr>
                                        <td align="right" width="50%"><b>Total IPK</b></td>
                                        <td><B>{{ $mhsw->IPK }}</B></td>
                                    </tr>
                                    <tr>
                                        <td align="right" width="50%"><b>Total SKS</b></td>
                                        <td><B>{{ $mhsw->TotalSKS }}</B></td>
                                    </tr>
                                    </table>
                                </li>
                            </ul>
                        </div>
                        {{-- DATA MATAKULIAH --}}
                        <div class="tab-pane" id="tab5">
                            <table class="table table-responsive">
                            <thead>
                                <th width="5%">No.</th>
                                <th width="15%">Kode</th>
                                <th width="60%">Mata Kuliah</th>
                                <th width="10%">SKS</th>
                                <th width="10%">Nilai</th>
                                <th width="10%">Bobot</th>
                            </thead>
                            <tbody>
                            @foreach($krs_detail as $key=>$krs)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $krs->MKKode }}</td>
                                <td>{{ $krs->Nama }}</td>
                                <td>{{ $krs->SKS }}</td>
                                <td>{{ $krs->GradeNilai }}</td>
                                <td>{{ $krs->BobotNilai }}</td>
                            </tr>
                            @endforeach
                            <tr>
                                <td align="right" colspan="5" width="50%"><b>Total IPK</b></td>
                                <td><B>{{ $mhsw->IPK }}</B></td>
                            </tr>
                            <tr>
                                <td align="right" colspan="5" width="50%"><b>Total SKS</b></td>
                                <td><B>{{ $mhsw->TotalSKS }}</B></td>
                            </tr>
                            </tbody>
                            </table>
                            <table class="table table-bordered">

                            </table>
                        </div>
                        <div class="tab-pane" id="tab6">
                            {{-- TAB DATA PRESTASI --}}
                        </div>
                        <div class="tab-pane" id="tab7">
                            {{-- TAB DATA TUGAS AKHIR --}}
                        </div>
                        <div class="tab-pane" id="tab8">
                            <table class="table table-responsive table-bordered">
                            <thead>
                                <th style="text-align:center" width="5%">Sesi</th>
                                <th style="text-align:center" width="5%">Tahun</th>
                                <th style="text-align:center" width="15%">Biaya</th>
                                <th style="text-align:center" width="15%">Potongan</th>
                                <th style="text-align:center" width="15%">Pembayaran</th>
                                <th style="text-align:center" width="15%">Tarikan</th>
                                <th style="text-align:center" width="15%">Saldo Akhir</th>
                            </thead>
                            <tbody>
                            @foreach($krs_header as $key =>$krs)
                            <?php $total = (-1*$krs->Biaya) + $krs->Potongan + $krs->Bayar - $krs->Tarik; ?>
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td align="right">{{ $krs->TahunID }}</td>
                                <td align="right">{{ number_format($krs->Biaya) }}</td>
                                <td align="right">{{ number_format($krs->Potongan) }}</td>
                                <td align="right">{{ number_format($krs->Bayar) }}</td>
                                <td align="right">{{ number_format($krs->Tarik) }}</td>
                                <td align="right" class="{{ $total >= 0 ? 'success' : 'danger' }}">{{ number_format($total) }}</td>
                            </tr>
                            @endforeach
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
