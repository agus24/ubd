@extends('layouts.app')

@section('content')
<br>
<h1 class="text-primary">Jadwal Ujian</h1>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="select">Tahun</label>
                        <select id="select" class="form-control">
                            <option value="">Select</option>
                            @foreach($allTahun as $tahunOpt)
                                <option value="{{ $tahunOpt }}" {{ ($tahunOpt == (!empty($_GET['tahun'])? $_GET['tahun'] : ""))? "selected" : "" }}>{{$tahunOpt}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-head">
                            <ul class="nav nav-tabs" data-toggle="tabs">
                                <li class="active"><a href="#uts">UTS</a></li>
                                <li><a href="#uas">UAS</a></li>
                            </ul>
                        </div>
                        <div class="card-body tab-content">
                            <div class="tab-pane active" id="uts">
                                <ul class="list divider-full-bleed">
                                    <div class="card panel">
                                        <div class="card-head style-primary " data-toggle="" data-parent="#semester{{ $tahun }}" data-target="#semester{{ $tahun }}">
                                            <header>UTS Tahun : {{ $tahun }}</header>
                                        </div>
                                        <div id="semester{{$tahun}}" class="">
                                            <div class="card-body">
                                                <table class="table table-responsive">
                                                    <thead>
                                                        <th>No.</th>
                                                        <th>Tanggal</th>
                                                        <th>Jam</th>
                                                        <th>Kode</th>
                                                        <th>Mata Kuliah</th>
                                                        <th>Ruang</th>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($jadwal['uts'] as $key=> $uts)
                                                            <tr>
                                                                <td>{{ $key+1 }}</td>
                                                                <td>{{ $uts->Tanggal }}</td>
                                                                <td>{{ $uts->JamMulai." - ".$uts->JamSelesai }}</td>
                                                                <td>{{ $uts->MKKode }}</td>
                                                                <td>{{ $uts->Nama }}</td>
                                                                <td>{{ $uts->RuangID }}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </ul>
                            </div>
                            <div class="tab-pane" id="uas">
                                <ul class="list divider-full-bleed">
                                    <div class="card panel">
                                        <div class="card-head style-primary " data-toggle="" data-parent="#semester{{ $tahun }}" data-target="#semester{{ $tahun }}">
                                            <header>UAS Tahun : {{ $tahun }}</header>
                                        </div>
                                        <div id="semester{{$tahun}}" class="">
                                            <div class="card-body">
                                                <table class="table table-responsive">
                                                    <thead>
                                                        <th>No.</th>
                                                        <th>Tanggal</th>
                                                        <th>Jam</th>
                                                        <th>Kode</th>
                                                        <th>Mata Kuliah</th>
                                                        <th>Ruang</th>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($jadwal['uas'] as $key=> $uas)
                                                            <tr>
                                                                <td>{{ $key+1 }}</td>
                                                                <td>{{ $uas->Tanggal }}</td>
                                                                <td>{{ $uas->JamMulai." - ".$uas->JamSelesai }}</td>
                                                                <td>{{ $uas->MKKode }}</td>
                                                                <td>{{ $uas->Nama }}</td>
                                                                <td>{{ $uas->RuangID }}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
        </div>
    </div>
</div>


@endsection
@section('script')
<script>
$('#select').on('change', function() {
    window.location.href = "?tahun="+this.value;
});
</script>
@endsection
