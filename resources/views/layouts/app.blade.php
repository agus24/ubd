<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{ Config::get('app.name') }}</title>

    <!-- BEGIN META -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="your,keywords">
    <meta name="description" content="Short explanation about this website">
    <!-- END META -->

    <!-- BEGIN STYLESHEETS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>

    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/'.Config::get('app.theme').'/bootstrap.css?1422792965') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/'.Config::get('app.theme').'/materialadmin.css?1425466319') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/'.Config::get('app.theme').'/font-awesome.min.css?1422529194') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/'.Config::get('app.theme').'/material-design-iconic-font.min.css?1421434286') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/'.Config::get('app.theme').'/libs/rickshaw/rickshaw.css?1422792967') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/'.Config::get('app.theme').'/libs/morris/morris.core.css?1420463396') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/'.Config::get('app.theme').'/libs/select2/select2.css?1424887856') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/'.Config::get('app.theme').'/libs/multi-select/multi-select.css?1424887857') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/'.Config::get('app.theme').'/libs/bootstrap-datepicker/datepicker3.css?1424887858') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/'.Config::get('app.theme').'/libs/jquery-ui/jquery-ui-theme.css?1423393666') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/'.Config::get('app.theme').'/libs/bootstrap-colorpicker/bootstrap-colorpicker.css?1424887860') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/'.Config::get('app.theme').'/libs/bootstrap-tagsinput/bootstrap-tagsinput.css?1424887862') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/'.Config::get('app.theme').'/libs/typeahead/typeahead.css?1424887863') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/'.Config::get('app.theme').'/libs/dropzone/dropzone-theme.css?1424887864') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/theme-custom/libs/DataTables/jquery.dataTables.css?1423553989') }}" />
<style>
.table th{
    font-weight: bold !important;
}
</style>
@yield('style')
<script>
    var Laravel = {}
    Laravel._token = "{{ csrf_token() }}";
</script>
    <!-- END STYLESHEETS -->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="../../assets/js/libs/utils/html5shiv.js?1403934957"></script>
    <script type="text/javascript" src="../../assets/js/libs/utils/respond.min.js?1403934956"></script>
    <![endif]-->
</head>

<body class="menubar-hoverable header-fixed ">
@if(Session::has('user'))
    @include('layouts.header')
    <div id="base">
        <div class="offcanvas">
            @yield('offcanvas-kiri')
        </div>
        <div id="content">
            <section>
                <div id="section-body">
                    @yield('content')
                </div>
            </section>
        </div>
        @include('layouts.sidebar')
        <div class="offcanvas">
            @yield('offcanvas-kanan')
        </div>
    </div>
@endif

@yield('login')
@yield('modal')
        <!-- BEGIN JAVASCRIPT -->
        <script src="{{ asset('assets/js/libs/jquery/jquery-1.11.2.min.js') }}"></script>
        <script src="{{ asset('assets/js/libs/jquery/jquery-migrate-1.2.1.min.js') }}"></script>
        <script src="{{ asset('assets/js/libs/bootstrap/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/js/libs/spin.js/spin.min.js') }}"></script>
        <script src="{{ asset('assets/js/libs/autosize/jquery.autosize.min.js') }}"></script>

        <script src="{{ asset('assets/js/libs/select2/select2.min.js') }}"></script>
        <script src="{{ asset('assets/js/libs/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
        <script src="{{ asset('assets/js/libs/multi-select/jquery.multi-select.js') }}"></script>
        <script src="{{ asset('assets/js/libs/inputmask/jquery.inputmask.bundle.min.js') }}"></script>
        <script src="{{ asset('assets/js/libs/bootstrap-datepicker/bootstrap-datepicker.js') }}"></script>
        <script src="{{ asset('assets/js/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js') }}"></script>
        <script src="{{ asset('assets/js/libs/typeahead/typeahead.bundle.min.js') }}"></script>
        <script src="{{ asset('assets/js/libs/dropzone/dropzone.min.js') }}"></script>
        <script src="{{ asset('assets/js/libs/moment/moment.min.js') }}"></script>

        <script src="{{ asset('assets/js/libs/flot/jquery.flot.min.js') }}"></script>
        <script src="{{ asset('assets/js/libs/flot/jquery.flot.time.min.js') }}"></script>
        <script src="{{ asset('assets/js/libs/flot/jquery.flot.resize.min.js') }}"></script>
        <script src="{{ asset('assets/js/libs/flot/jquery.flot.orderBars.js') }}"></script>
        <script src="{{ asset('assets/js/libs/flot/jquery.flot.pie.js') }}"></script>
        <script src="{{ asset('assets/js/libs/flot/curvedLines.js') }}"></script>
        <script src="{{ asset('assets/js/libs/jquery-knob/jquery.knob.min.js') }}"></script>
        <script src="{{ asset('assets/js/libs/sparkline/jquery.sparkline.min.js') }}"></script>
        <script src="{{ asset('assets/js/libs/nanoscroller/jquery.nanoscroller.min.js') }}"></script>
        <script src="{{ asset('assets/js/libs/d3/d3.min.js') }}"></script>
        <script src="{{ asset('assets/js/libs/d3/d3.v3.js') }}"></script>
        <script src="{{ asset('assets/js/libs/rickshaw/rickshaw.min.js') }}"></script>
        <script src="{{ asset('assets/js/libs/DataTables/jquery.dataTables.min.js') }}"></script>

        <script src="{{ asset('assets/js/core/source/App.js') }}"></script>
        <script src="{{ asset('assets/js/core/source/AppNavigation.js') }}"></script>
        <script src="{{ asset('assets/js/core/source/AppOffcanvas.js') }}"></script>
        <script src="{{ asset('assets/js/core/source/AppCard.js') }}"></script>
        <script src="{{ asset('assets/js/core/source/AppForm.js') }}"></script>
        <script src="{{ asset('assets/js/core/source/AppNavSearch.js') }}"></script>
        <script src="{{ asset('assets/js/core/source/AppVendor.js') }}"></script>

        <script src="{{ asset('assets/js/core/demo/Demo.js') }}"></script>
        <!-- END JAVASCRIPT -->
        @yield('script')

    </body>
</html>
