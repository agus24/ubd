<div id="menubar" class="menubar-inverse ">
    <div class="menubar-fixed-panel">
        <div>
            <a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                <i class="fa fa-bars"></i>
            </a>
        </div>
        <div class="expanded">
            <a href="../../html/dashboards/dashboard.html">
                <span class="text-lg text-bold text-primary ">{{ Config::get('app.name') }}</span>
            </a>
        </div>
    </div>
    <div class="menubar-scroll-panel">
        <!-- BEGIN MAIN MENU -->
        <ul id="main-menu" class="gui-controls">
            <!-- BEGIN UI -->
            <li class="gui-folder">
                <a>
                    <div class="gui-icon"><i class="fa fa-user fa-fw"></i></div>
                    <span class="title">Dosen</span>
                </a>
                <!--start submenu -->
                <ul>
                    <li><a href="{{ url('/dosen/data') }}" ><span class="title">Data Diri</span></a></li>
                    <li><a href="{{ url('/dosen/jadwalMatakuliah') }}" ><span class="title">Jadwal Mengajar</span></a></li>
                    <li><a href="{{ url('/dosen/nilaiMatakuliah') }}" ><span class="title">Nilai Matakuliah</span></a></li>
                    <li><a href="{{ url('/dosen/jadwalUjian') }}" ><span class="title">Jadwal Ujian</span></a></li>
                </ul><!--end /submenu -->
            </li>
            <li class="gui-folder">
                <a>
                    <div class="gui-icon"><i class="fa fa-user fa-fw"></i></div>
                    <span class="title">Mahasiswa</span>
                </a>
                <!--start submenu -->
                <ul>
                    <li><a href="{{ url('/mahasiswa/data') }}" ><span class="title">Data Mahasiswa</span></a></li>
                    <li><a href="{{ url('/mahasiswa/krs') }}" ><span class="title">Kartu Rencana Studi</span></a></li>
                    <li><a href="{{ url('/mahasiswa/nilaiSemester') }}" ><span class="title">Nilai Semester</span></a></li>
                    <li><a href="{{ url('/mahasiswa/jadwalUjian') }}" ><span class="title">Jadwal Ujian</span></a></li>
                </ul><!--end /submenu -->
            </li>

        </ul><!--end .main-menu -->
        <!-- END MAIN MENU -->

        {{-- <div class="menubar-foot-panel">
            <small class="no-linebreak hidden-folded">
                <span class="opacity-75">Copyright &copy; 2017</span> <strong>Gustiawan Ouwawi</strong>
            </small>
        </div> --}}
    </div><!--end .menubar-scroll-panel-->
</div><!--end #menubar-->
