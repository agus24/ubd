<header id="header" >
    <div class="headerbar">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="headerbar-left">
            <ul class="header-nav header-nav-options">
                <li class="header-nav-brand" >
                    <div class="brand-holder">
                        <a href="{{ url('/') }}">
                            <span class="text-lg text-bold text-primary">{{ Config::get('app.name') }}</span>
                        </a>
                    </div>
                </li>
                <li>
                    <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                        <i class="fa fa-bars"></i>
                    </a>
                </li>
            </ul>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="headerbar-right">
            <ul class="header-nav header-nav-profile">
                <li class="dropdown">
                    <a class="dropdown-toggle ink-reaction" href="javascript:void(0);" data-toggle="dropdown">
                        <span class="text-lg text-bold">
                            Pengumuman
                            <sup class="badge style-danger"></sup>
                            {{-- <small>Administrator</small> --}}
                        </span>
                    </a>
                    <ul class="dropdown-menu animation-dock">
                        <li>
                            <a class="ink-reaction btn-default-bright" href="{{ url('info/akademik') }}">Akademik</a>
                        </li>
                        <div class="divider"></div>
                        <li><a class="ink-reaction btn-default-bright" href="{{ url('info/perkuliahan') }}">Perkuliahan</a></li>
                        <div class="divider"></div>
                        <li><a class="ink-reaction btn-default-bright" href="{{ url('info/keuangan') }}">Keuangan</a></li>
                        <li></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle ink-reaction" data-toggle="dropdown">
                        <span class="text-lg text-bold">
                            {{ Session::get('user')->nama }}
                        </span>
                    </a>
                    <ul class="dropdown-menu animation-dock">
                        <li><a href="{{ url('/logout') }}"><i class="fa fa-fw fa-power-off text-danger"></i> Logout</a></li>
                    </ul><!--end .dropdown-menu -->
                </li><!--end .dropdown -->
            </ul><!--end .header-nav-profile -->
        </div><!--end #header-navbar-collapse -->
    </div>
</header>
