<div id="offcanvas-perkuliahan" class="offcanvas-pane width-12" style="width:800px">
    <div class="offcanvas-head">
        <header>Info Perkuliahan</header>
        <div class="offcanvas-tools">
            <a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
                <i class="md md-close"></i>
            </a>
        </div>
    </div>
    <div class="offcanvas-body">
        <div class="panel-group" id="accordion7">
        @foreach($pengumuman['infoAkademik'] as $key => $akademik)
            <div class="card panel">
                <div class="card-head style-primary-light collapsed" data-toggle="collapse" data-parent="#accordion6" data-target="#accordion6-{{$key}}" aria-expanded="false">
                    <div style="margin-left:1%">
                        {{ $akademik->judul_infoakademik }}
                    </div>
                    <div class="tools">
                        <div class="btn-group">
                        </div>
                        <a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
                    </div>
                </div>
                <div id="accordion6-{{$key}}" class="collapse" aria-expanded="false" style="height: 0px;">
                    <div class="card-body" style="overflow-y: scroll;">
                        {!! $akademik->isi_infoakademik !!}
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>
    <div class="force-padding stick-bottom-right">
        <a class="btn btn-floating-action btn-accent " href="#offcanvas-demo-size3" data-toggle="offcanvas">
            <i class="md md-arrow-back"></i>
        </a>
    </div>
</div>
