@extends('layouts.app')

@section('content')
<br>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-head">
                <header>Info {{ $judul }}</header>
            </div>
            <div class="card-body">
                <table class="table table-responsive">
                    <thead>
                        <th>No.</th>
                        <th>Judul</th>
                        <th>Dibuat Oleh</th>
                        <th>Tanggal</th>
                    </thead>
                    <tbody>
                    {{ Log::info("Loop Start")}}
                    @foreach($pengumumans as $key => $pengumuman)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td><a class="btn btn-raised ink-reaction btn-default-bright" href="#pengumuman{{$pengumuman->id}}" data-toggle="offcanvas">{{ $pengumuman->judul }}</a></td>
                        <td>{{ $pengumuman->penulis }}</td>
                        <td>{{ $pengumuman->tanggal }}</td>
                    </tr>
                    {{ Log::info($key." data") }}
                    @endforeach
                    {{ Log::info("Loop end") }}
                    </tbody>
                </table>
                {{ Log::info('Pagination start')}}
                <div class="pagination-wrapper"> {!! $pengumumans->appends(['search' => Request::get('search')])->render() !!} </div>
                {{ Log::info('Pagination end')}}
            </div>
        </div>
    </div>
</div>

@endsection

@section('offcanvas-kiri')
    {{ Log::info('left offcanvas start')}}
    {{ Log::info('looping start')}}
    @foreach($pengumumans as $key => $pengumuman)
    <div id="pengumuman{{$pengumuman->id}}" class="offcanvas-pane width-12" style="width:1000px">
        <div class="offcanvas-head">
            <header>{{ $pengumuman->judul }}</header>
            <div class="offcanvas-tools">
                <a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
                    <i class="md md-close"></i>
                </a>
            </div>
        </div>
        <div class="offcanvas-body" style="overflow-y:scroll;height:100%">
            <div class="divider"></div>
            {!! $pengumuman->isi !!}
            {{ Log::warning($pengumuman->isi) }}
        </div>
    </div>
    {{ Log::info($key." Data") }}
    @endforeach
      <div id="pengumuman" class="offcanvas-pane width-12" style="width:1000px">
        <div class="offcanvas-head">
            <header></header>
            <div class="offcanvas-tools">
                <a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
                    <i class="md md-close"></i>
                </a>
            </div>
        </div>
        <div class="offcanvas-body" style="overflow-y:scroll;height:100%">
            <div class="divider"></div>
            <h2 style="text-align: center;">Daftar Mahasiswa Kurang Bayar Prodi Akuntansi Tahun Akademik 20152</h2>

        </div>
    </div>
    {{ Log::info('looping end')}}
@endsection
